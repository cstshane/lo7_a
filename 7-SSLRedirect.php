<?php
if ( !isset( $_SERVER["HTTPS"] ) || $_SERVER["HTTPS"] == "" )
{
    // Redirect user to the secure version of the requested page
    // Tell the browser the page has moved for good
    header( "HTTP/1.1 301 Moved Permanently" );
    
    // Now tell the browser where the new location is
    // The new location is simply https:// instead of http://
    header( "Location: https://" . $_SERVER["HTTP_HOST"] .
            $_SERVER["REQUEST_URI"] );
    
    // Finally we tell PHP to stop all further processing
    exit();
}
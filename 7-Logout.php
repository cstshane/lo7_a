<?php

session_start();

// Log out
$_SESSION["loggedIn"] = FALSE;
//unset( $_SESSION["loggedIn"] );
//session_unset();
//session_destroy();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Website Logout Page</title>
    </head>
    <body>
        <p>You have been logged out</p>
        <p><a href="7-Login.php">Click here</a>
            to log in again</p>
    </body>
</html>

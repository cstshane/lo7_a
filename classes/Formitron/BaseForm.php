<?php

namespace Formitron;

use Formitron\Element\BaseElement;

/**
 * Class to represent a basic HTML form. Method and action can be specified,
 * and instances of BaseElement can be added to the form
 */
class BaseForm
{
    const METHOD_GET = "GET";
    const METHOD_POST= "POST";
    
    protected $method;
    protected $action;
    protected $properties;
    
    //The set of BaseElements added to this form
    protected $elements=array();
    
    /**
     * Create a new form. Default method is GET, default action is empty
     * @param string $method The HTTP method to use when submitting the form
     * @param string $action The value to place in the action property of the form
     * @param type $properties
     */
    public function __construct($method=self::METHOD_GET, $action="", $properties=array()  )
    {
	$this->method = $method;
	$this->action = $action;
	$this->properties = $properties;
    }
    
    /**
     * Add an instance of BaseElement to the form, optionally at a given index
     * @param BaseElement $element
     * @param mixed $index The index within the elements array at which the new
     * element should be placed
     */
    public function add(BaseElement $element, $index=null)
    {
	if(is_null($index))
	{
	    $this->elements[] = $element;
	}
	else
	{
	    $this->elements[$index] = $element;
	}
    }
    
    public function remove(BaseElement $element)
    {
	$result = array_search($element, $this->elements);
	if($result !== false)
	{
	    unset($this->elements[$result]);
	}
    }
    
    /**
     * Render the form. Ideally would use some form of templating. Converts the
     * data within this instance to a HTML form tag, rendering each inner
     * element in turn
     * @return string The rendered form
     */
    public function render()
    {
	//Initial form tag
	$formString = "<form action='{$this->action}' method='{$this->method}' ";
	//Add property key="value" pairs to the form tag
	foreach($this->properties as $key=>$value)
	{
	    $value = htmlspecialchars($value);
	    $formString .= "${key}=\"$value\" ";
	}
	//End the form open tag
	$formString .= ">\n"; 
	
	//Render each element within the form
	foreach($this->elements as $element)
	{
	    $formString .= $element->render() ."\n\n";
	}
	$formString .= "<input type='hidden' name='_submitted' value='1' />\n";
	//End the form
	$formString .= "</form>\n";
	return $formString;
    }
    
    /**
     * If the form has been submitted, tell each element in turn to perform
     * whatever action it wants to perform with the submitted data. Must be called
     * by the user after adding all form elements, but before calling render.
     */
    public function handleSubmit()
    {
	$formData = $this->method==self::METHOD_GET?$_GET:$_POST;

	//Get values from GET or POST depending on the configuration of this form
	if($this->wasSubmitted($formData))
	{
	    foreach($this->elements as $element)
	    {
		//Tell each element in turn to load its own value from get/post
		$element->handleSubmit($formData);
	    }
	}
    }
    
    public function wasSubmitted($formData)
    {
	return isset($formData['_submitted']);
	
    }
    
    /**
     * Retrieves a value from the GET or POST superglobal, depending on which one
     * was passed into this form when it was instantiated
     * @param type $name The name of the index in GET or POST to return a value from
     * @param mixed $default Optional default value to return if no value was found
     * @return mixed The value found at index $name in either GET or POST, or the
     * $default value if nothing was found at that index
     */
    public function getValueFrom($name, $default=null)
    {
	$formData = $this->method==self::METHOD_GET?$_GET:$_POST;
	if(isset($formData[$name]))
	{
	    return $formData[$name];
	}
	return $default;
    }
    
    
}


<?php
namespace Formitron;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Formitron\BaseForm;
/**
 * Subclass of BaseForm which adds in an enctype attribute, allowing for file
 * uploads to PHP.
 *
 * @author ins208
 */
class FileForm extends BaseForm
{
    //put your code here
    public function __construct($action = "", $properties = array())
    {
	//Add enctype to the set of properties
	$properties['enctype']='multipart/form-data';
	parent::__construct(self::METHOD_POST, $action, $properties);
    }
}

<?php
namespace Formitron\Element;

use Formitron\Element\BaseElement;

/**
 * Implementation of an individual checkbox element
 *
 * @author ins208
 */
class Checkbox extends BaseElement
{
    
    /**
     * 
     * @param type $name The html name of this item
     * @param type $value The value of this item, when checked
     * @param type $checked Whether or not this item should be checked
     * @param string $properties
     */
    public function __construct($name, $value, $checked=false, $properties = array())
    {
	$properties['value']=$value;
	
	if(!isset($properties['id']))
	{
	    $properties['id']=$name;
	}
	
	$properties['name'] = $name;
	
	$properties['type'] = "checkbox";
	
	if($checked)
	{
	    $properties['checked']='checked';
	}
	
	parent::__construct("input", $properties);
    }
    
    public function check()
    {
	$this->properties['checked'] = 'checked';
    }
    
    public function uncheck()
    {
	if(isset($this->properties['checked']))
	{
	    unset($this->properties['checked']);
	}
    }
    
    /**
     * Load the checked state of this box from either GET or POST data.
     * Since checkboxes don't exist in the submitted form unless they're checked, 
     * we set the default state of the checkbox to unchecked, then only "check" it
     * if the value is present in formData
     * @param type $formData
     */
    public function handleSubmit($formData)
    {
	$name = $this->properties['name'];

	unset($this->properties['checked']);

	if(isset($formData[$name]))
	{
	    $this->properties['checked']='checked';
	}
    }
}

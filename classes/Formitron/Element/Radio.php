<?php
namespace Formitron\Element;
use Formitron\Element\BaseElement;
/**
 * Implementation of a Radio button
 *
 * @author ins208
 */
class Radio extends BaseElement
{
    /**
     * 
     * @param type $name Form name of the element
     * @param type $value Value of the element
     * @param type $checked Whether or not the element should default to selected
     * @param array $properties Set of kvp for other tag properties
     */
    public function __construct($name, $value, $checked=false, $properties = array())
    {
	$properties['value']=$value;
	
	if(!isset($properties['id']))
	{
	    $properties['id']=$name;
	}
	
	$properties['name'] = $name;
	
	$properties['type'] = "radio";
	
	if($checked)
	{
	    $properties['checked']='checked';
	}
	
	parent::__construct("input", $properties);
    }
    
    /**
     * Check or uncheck this item
     * @param type $to True if the item should be checked, false otherwise;
     */
    public function setChecked($to)
    {
	if($to)
	{
	    unset($this->properties['checked']);
	}
	else
	{
	    $this->properties['checked']='checked';
	}
    }
    
    /**
     * Reload the checked/unchecked state for this item.
     * @param type $source either GET or POST superglobal
     */
    public function handleSubmit($formData)
    {
	if(isset($formData[$this->properties['name']]))
	{
	    unset($this->properties['checked']);
	    $value = $formData[$this->properties['name']];
	    if($this->properties['value'] == $value)
	    {
		$this->properties['checked']='checked';
	    }
	}
    }
    
    
}

<?php
namespace Formitron\Element;

use Formitron\Element\BaseElement;
/**
 * Represents a HTML label element, for a given HTML form element
 *
 * @author ins208
 */
class Label extends BaseElement
{
    protected $text;
    
    
    /**
     * 
     * @param type $for The name of the element this label should be attached to
     * @param type $text The text which should appear in this label. If not set,
     * text will be the ucfirst form of $for
     * @param array $properties Array of kvp for other properties of this tag
     */
    public function __construct($for, $text=null, $properties = array())
    {
	if(is_null($text))
	{
	    $this->text = ucfirst($for);
	}
	else
	{
	    $this->text = $text;
	}

	
	$properties['for']=$for;
	
	parent::__construct("label", $properties);
    }
    
    
    protected function renderInnerHTML()
    {
	return $this->text;
    }
    
    public function handleSubmit($formData)
    {
	;
    }

//put your code here
}

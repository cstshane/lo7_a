<?php
namespace Formitron\Element;

use Formitron\Element\Label;
/**
 * Represents a HTML label element, for a given HTML form element.
 * Rather than the label being separate from the element it represents, this
 * label wraps around the element it represents (ie Bootstrap radio and checkbox)
 *
 * @author ins208
 */
class ContainerLabel extends Label
{
    protected $text;
       
    protected $innerElement;
    
    /**
     * 
     * @param string $for The name of the element this label should be attached to
     * @param string $text The text which should appear in this label. If not set,
     * text will be the ucfirst form of $for
     * @param array $properties Array of kvp for other properties of this tag
     * @param BaseElement $innerElement The element this label will wrap around
     */
    public function __construct($for, BaseElement $innerElement, $text=null, $properties = array())
    {
	$this->innerElement = $innerElement;
	parent::__construct($for, $text, $properties);
    }
    
    /**
     * Create the inner representation of this label (between <label> and </label> tags)
     * This will be the inner element itself, plus the text of this label
     * @return string 
     */
    protected function renderInnerHTML()
    {
	
	return "\n" . $this->innerElement->renderSelf() . parent::renderInnerHTML() . "\n";
	
    }
    
    /**
     * This label has no data to load, but its inner element does
     * @param type $source
     */
    public function handleSubmit($formData)
    {
	$this->innerElement->handleSubmit($formData);
    }
    
    /**
     * Wrapper labels can never have a short tag, so always return false.
     * @return boolean
     */
    public function canHaveShortTag()
    {
	return false;
    }

//put your code here
}

<?php
namespace Formitron\Element;

use Formitron\Element\BaseElement;

/**
 * Description of TextArea
 *
 * @author ins208
 */
class TextArea extends BaseElement
{
    private $value;
    
    public function __construct( $name,  $value=null,  $properties = array())
    {
	if(!isset($properties['id']))
	{
	    $properties['id']=$name;
	}
	
	$properties['name'] = $name;
	
	$properties['class'] = "form-control";
	
	$this->value = $value;

	parent::__construct("textarea", $properties);
    }
    
    	
    protected function renderInnerHTML()
    {
	return htmlspecialchars($this->value);
    }
    
    protected function canHaveShortTag()
    {
	return false;
    }
    
    public function handleSubmit($formData)
    {
	if(isset($formData[$this->properties['name']]))
	{
	    $this->value = $formData[$this->properties['name']];
	}
    }
}

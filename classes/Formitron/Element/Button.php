<?php
namespace Formitron\Element;

use Formitron\Element\BaseElement;
/**
 * Description of Button
 *
 * @author ins208
 */
class Button extends BaseElement
{
 
    const TYPE_SUBMIT="submit";    
    const TYPE_RESET="reset";
    
    public function __construct($name, $label,  $type="button", $properties = [])
    {
	$properties['value']=$label;
	
	if(!isset($properties['id']))
	{
	    $properties['id']=$name;
	}	
	$properties['name'] = $name;
	
	$properties['class'] = "btn";
	
	$properties['type'] = $type;
	
	parent::__construct("input", $properties);
    }
    
    /**
     * Buttons cannot load a value from GET or POST
     * @param type $formData
     */
    public function handleSubmit($formData)
    {
	;
    }
}

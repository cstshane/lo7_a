<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Formitron\Element;


use Formitron\Element\BaseElement;
/**
 * Description of Hidden
 *
 * @author ins208
 */
class Hidden extends BaseElement
{
    public function __construct( $name,  $value=null, $properties = array())
    {
	if(!isset($properties['id']))
	{
	    $properties['id']=$name;
	}	
	$properties['name'] = $name;
	
	$properties['value'] = $value;
	
	$properties['type'] = "hidden";
	
	$properties['class'] = " form-control";
	
	parent::__construct("input", $properties);
    }


}

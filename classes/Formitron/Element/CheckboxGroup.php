<?php
namespace Formitron\Element;

use Formitron\Element\Group;

/**
 * Represents a group of checkboxes. Checkboxes are wrapped in standard bootstrap
 * div classes for formatting.
 *
 * @author ins208
 */
class CheckboxGroup extends Group
{
    //Text label which will appear at the start of the group
    protected $label;
    
    //All of the checkboxes in this group, with key being the value of the
    // checkbox when set, the value being the checkbox itself
    protected $checkboxes = array();
    
    
    protected $name;
    
    /**
     * 
     * @param string $name Form name for this group of checkboxes
     * @param string $label Label for the group
     * @param array $checkedValues Array of values which should be "checked"
     * @param array $options Array of key value pairs for the checkboxes to be displayed. 
	 * Keys in this array represent the label for a checkbox item, associated values are 
	 * used as the value attribute of the generated Checkbox input
     */
    public function __construct($name, $label, $checkedValues, $options=array())
    {
	$this->label = $label;
	$this->wrapClass = "form-group check-group";
	$this->name = $name;
	foreach($options as $label=>$value)
	{
	    $itemGroup = new Group();
	    $itemGroup->wrapGroupWith("div", "checkbox");
	    
	    //Each checkbox, even though all are named the same, should have
	    // a unique identifier so that it could be manipulated using javascript
	    // We generate a name by appending the name for the entire group to
	    // the value of this one checkbox, then removing all non-alphanumeric
	    $identifier = preg_replace("/[^a-zA-Z0-9]/", '', $name . $value);

	    //The checkbox should be checked if its value is present in the
	    // checkedValues array
	    $isChecked = in_array($value, $checkedValues);
	    
	    //When multiple checkboxes are present, PHP expects them to be named
	    // $name[]. GET/POST will contain an array at METHOD[name] with all
	    // of the values of the "checked" checkboxes
	    $cb = new Checkbox($name."[]", $value, $isChecked,array('id'=>$identifier));
	    
	    //Keep track of all of the checkboxes so that we can later set their
	    // checked/unchecked state from submitted form data
	    $this->checkboxes[$value] = $cb;
	    
	    //Each checkbox needs a label wrapped around it
	    $wrapperLabel = new ContainerLabel($identifier, $cb, $label);
	    $itemGroup->add($wrapperLabel);
	    
	    $this->add($itemGroup);
	}
	
	parent::__construct();
    }
    
    protected function renderInnerHTML()
    {
	
	return "\n<strong>{$this->label}</strong><br />" . parent::renderInnerHTML();
    }
    
    
    /**
     * Restore the checked/unchecked state of each checkbox from form data
     * @param type $formData GET or POST superglobal
     */
    public function handleSubmit($formData)
    {
	//Default each checkbox to unchecked
	foreach($this->checkboxes as $box)
	{
	    $box->uncheck();
	}
	
	//Now that all checkboxes are checked, we can set only those which
	// appear in the form data to checked, since only checked box values
	// are sent in the form data
	if(isset($formData[$this->name]) && is_array($formData[$this->name]))
	{
	    foreach($formData[$this->name] as $value)
	    {
		if(isset($this->checkboxes[$value]))
		{
		    $box = $this->checkboxes[$value];
		    $box->check();
		}
	    }
	}
    }

    
}

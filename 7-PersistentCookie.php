<?php

setcookie( "user", "Kotter", time()+60 );

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Set a persistent cookie</title>
    </head>
    <body>
        <h1>Set a persistent cookie</h1>
        <p>Your persistent cookie has been set</p>
    </body>
</html>
